/*
 * Copyright (C) 2015 EasyFonts
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.vstechlab.easyfonts;



import ohos.agp.text.Font;
import ohos.app.Context;
import com.vstechlab.easyfonts.ResourceTable;


/**
 * Wraps Font object creation.
 * Developer should not have to worry about adding fonts in asset folder.
 *
 * @author vijay.s.vankhede@gmail.com (Vijay Vankhede)
 */
public final class EasyFonts {

    private EasyFonts(){}

    /**
     * Roboto-Thin font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Thin
     */
    public static Font robotoThin(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_black, context);
    }


    /**
     * Roboto-Black font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Black
     */
    public static Font robotoBlack(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_black, context);
    }

    /**
     * Roboto-BlackItalic font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Black-Italic
     */
    public static Font robotoBlackItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_blackitalic, context);
    }

    /**
     * Roboto-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Bold
     */
    public static Font robotoBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_bold, context);
    }

    /**
     * Roboto-BoldItalic font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Bold-Italic
     */
    public static Font robotoBoldItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_bolditalic, context);
    }

    /**
     * Roboto-Italic font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Italic
     */
    public static Font robotoItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_italic, context);
    }

    /**
     * Roboto-Light font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Light
     */
    public static Font robotoLight(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_light, context);
    }

    /**
     * Roboto-LightItalic
     * @param  context the context that is used to get font
     * @return Font object for Roboto Light-Italic
     */
    public static Font robotoLightItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_lightitalic, context);
    }

    /**
     * Roboto-Medium font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Medium
     */
    public static Font robotoMedium(Context context){

        return FontSourceProcessor.process(ResourceTable.Profile_roboto_medium, context);
    }

    /**
     * Roboto-MediumItalic font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Medium-Italic
     */
    public static Font robotoMediumItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_mediumitalic, context);
    }

    /**
     * Roboto-Regular font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Regular
     */
    public static Font robotoRegular(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_regular, context);
    }

    /**
     * Roboto-ThinItalic font face
     * @param  context the context that is used to get font
     * @return Font object for Roboto Thin-Italic
     */
    public static Font robotoThinItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_roboto_thinitalic, context);
    }

    /**
     * Recognition font face
     * @param  context the context that is used to get font
     * @return Font object for Recognition
     */
    public static Font recognition(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_recognition, context);
    }

    /**
     * Android Nation font face
     * @param  context the context that is used to get font
     * @return Font object for Android Nation
     */
    public static Font androidNation(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_androidnation, context);
    }

    /**
     * Android Nation-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for Android Nation-Bold
     */
    public static Font androidNationBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_androidnation_b, context);
    }

    /**
     * Android Nation-Italic font face
     * @param  context the context that is used to get font
     * @return Font object for Android Nation-Italic
     */
    public static Font androidNationItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_androidnation_i, context);
    }

    /**
     * Droid Serif-Regular font face
     * @param  context the context that is used to get font
     * @return Font object for Droid Serif-Regular
     */
    public static Font droidSerifRegular(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_droidserif_regular, context);
    }

    /**
     * Droid Serif-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for Droid Serif-Bold
     */
    public static Font droidSerifBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_droidserif_bold, context);
    }

    /**
     * Droid Serif Bold-Italic font face
     * @param  context the context that is used to get font
     * @return Font object for Droid Serif Bold-Italic
     */
    public static Font droidSerifBoldItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_droidserif_bolditalic, context);
    }

    /**
     * Droid Serif-Italic font face
     * @param  context the context that is used to get font
     * @return Font object for Droid Serif-Italic
     */
    public static Font droidSerifItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_droidserif_italic, context);
    }

    /**
     * Freedom font face
     * @param  context the context that is used to get font
     * @return FontFont object for Freedom
     */
    public static Font freedom(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_freedom, context);
    }

    /**
     * Droid Robot font face
     * @param  context the context that is used to get font
     * @return Font object for Droid Robot
     */
    public static Font droidRobot(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_droid_robot_jp2, context);
    }

    /**
     * Fun Raiser font face
     * @param  context the context that is used to get font
     * @return Font object for Fun Raiser
     */
    public static Font funRaiser(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_fun_raiser, context);
    }

    /**
     * Green Avocado font face
     * @param  context the context that is used to get font
     * @return Font object for Green Avocado
     */
    public static Font greenAvocado(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_green_avocado, context);
    }

    /**
     * Walkway-Black font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-Black
     */
    public static Font walkwayBlack(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_black, context);
    }

    /**
     * Walkway-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-Bold
     */
    public static Font walkwayBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_bold, context);
    }

    /**
     * Walkway-Oblique font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-Oblique
     */
    public static Font walkwayOblique(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_oblique, context);
    }

    /**
     * Walkway-ObliqueBlack font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-ObliqueBlack
     */
    public static Font walkwayObliqueBlack(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_oblique_black, context);
    }

    /**
     * Walkway-ObliqueSemiBold font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-ObliqueSemiBold
     */
    public static Font walkwayObliqueSemiBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_oblique_semibold, context);
    }

    /**
     * Walkway-Oblique-UltraBold font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-Oblique-UltraBold
     */
    public static Font walkwayObliqueUltraBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_oblique_ultrabold, context);
    }

    /**
     * Walkway-SemiBold font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-SemiBold
     */
    public static Font walkwaySemiBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_semibold, context);
    }

    /**
     * Walkway-UltraBold font face
     * @param  context the context that is used to get font
     * @return Font object for Walkway-UltraBold
     */
    public static Font walkwayUltraBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_walkway_ultrabold, context);
    }

    /**
     * Windsong font face
     * @param  context the context that is used to get font
     * @return Font object for Windsong
     */
    public static Font windSong(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_windsong, context);
    }

    /**
     * Tangerine-Regular font face
     * @param  context the context that is used to get font
     * @return Font object for Tangerine-Regular
     */
    public static Font tangerineRegular(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_tangerine_regular, context);
    }

    /**
     * Tangerine-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for Tangerine-Bold
     */
    public static Font tangerineBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_tangerine_bold, context);
    }

    /**
     * Ostrich-Black font face
     * @param  context the context that is used to get font
     * @return Font object for Ostrich-Black
     */
    public static Font ostrichBlack(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_ostrich_black, context);
    }

    /**
     * Ostrich-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for Ostrich-Bold
     */
    public static Font ostrichBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_ostrich_bold, context);
    }

    /**
     * Ostrich-Dashed font face
     * @param  context the context that is used to get font
     * @return Font object for Ostrich-Dashed
     */
    public static Font ostrichDashed(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_ostrich_dashed, context);
    }

    /**
     * Ostrich-Light font face
     * @param  context the context that is used to get font
     * @return Font object for Ostrich-Light
     */
    public static Font ostrichLight(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_ostrich_light, context);
    }

    /**
     * Ostrich-Regular font face
     * @param  context the context that is used to get font
     * @return Font object for Ostrich-Regular
     */
    public static Font ostrichRegular(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_ostrich_regular, context);
    }

    /**
     * Ostrich-Rounded font face
     * @param  context the context that is used to get font
     * @return Font object for Ostrich-Rounded
     */
    public static Font ostrichRounded(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_ostrich_rounded, context);
    }

    /**
     * CaviarDreams-Bold font face
     * @param  context the context that is used to get font
     * @return Font object for CaviarDreams-Bold
     */
    public static Font caviarDreamsBold(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_caviar_dreams_bold, context);
    }

    /**
     * CaviarDreams font face
     * @param  context the context that is used to get font
     * @return Font object for CaviarDreams
     */
    public static Font caviarDreams(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_caviardreams, context);
    }

    /**
     * CaviarDreams-BoldItalic font face
     * @param  context the context that is used to get font
     * @return Font object for CaviarDreams-BoldItalic
     */
    public static Font caviarDreamsBoldItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_caviardreams_bolditalic, context);
    }

    /**
     * CaviarDreams-Italic font face
     * @param  context the context that is used to get font
     * @return Font object for CaviarDreams-Italic
     */
    public static Font caviarDreamsItalic(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_caviardreams_italic, context);
    }

    /**
     * Capture-It font face
     * @param  context the context that is used to get font
     * @return Font object for Capture-It
     */
    public static Font captureIt(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_capture_it, context);
    }

    /**
     * Capture-It font face
     * @param  context the context that is used to get font
     * @return Font object for Capture-It2
     */
    public static Font captureIt2(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_capture_it_2, context);
    }

    /**
     * CAC-Champagne font face
     * @param  context the context that is used to get font
     * @return Font object for CAC-Champagne
     */
    public static Font cac_champagne(Context context){
        return FontSourceProcessor.process(ResourceTable.Profile_cac_champagne, context);
    }
}