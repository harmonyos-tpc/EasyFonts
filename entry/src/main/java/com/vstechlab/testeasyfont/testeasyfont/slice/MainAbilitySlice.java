/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vstechlab.testeasyfont.testeasyfont.slice;

import com.vstechlab.easyfonts.EasyFonts;
import com.vstechlab.testeasyfont.testeasyfont.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class MainAbilitySlice extends AbilitySlice {
    // Roboto
    Text tvRobotoBlack;
    Text tvRobotoBlackItalic;
    Text tvRobotoBold;
    Text tvRobotoBoldItalic;
    Text tvRobotoItalic;
    Text tvRobotoLight;
    Text tvRobotoLightItalic;
    Text tvRobotoMedium;
    Text tvRobotoMediumItalic;
    Text tvRobotoRegular;
    Text tvRobotoThin;
    Text tvRobotoThinItalic;
    // Android Nation
    Text tvAndroidNation;
    Text tvAndroidNationBold;
    Text tvAndroidNationItalic;
    // Droid Robot
    Text tvDroidRobot;
    // Droid Serif
    Text tvDroidSerifBold;
    Text tvDroidSerifBoldItalic;
    Text tvDroidSerifItalic;
    Text tvDroidSerifRegular;

    Text tvFreedom;

    Text tvFunRaiser;

    Text tvGreenAvocado;

    Text tvRecognition;

    // Walkway
    Text tvWalkwayBlack;

    Button button;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);

        button = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_1);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                setupFonts();
            }
        });

        initview( rootLayout);
        super.setUIContent(rootLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }




    public void initview(ComponentContainer rootLayout) {

        tvRobotoBlack = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_black);
        tvRobotoBlackItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_black_italic);
        tvRobotoBold = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_bold);
        tvRobotoBoldItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_bold_italic);
        tvRobotoItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_italic);
        tvRobotoLight = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_light);
        tvRobotoLightItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_light_italic);
        tvRobotoMedium = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_medium);
        tvRobotoMediumItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_medium_italic);
        tvRobotoRegular = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_regular);
        tvRobotoThin = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_thin);
        tvRobotoThinItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_roboto_thin_italic);

        tvAndroidNation = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_ohos_nation);
        tvAndroidNationBold = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_ohos_nation_bold);
        tvAndroidNationItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_ohos_nation_italic);

        tvDroidRobot = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_droid_robot);

        tvDroidSerifBold = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_droid_serif_bold);
        tvDroidSerifBoldItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_droid_serif_bold_italic);
        tvDroidSerifItalic = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_droid_serif_italic);
        tvDroidSerifRegular = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_droid_serif_regular);

        tvFreedom = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_freedom);
        tvFunRaiser = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_fun_raiser);
        tvGreenAvocado = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_green_avocado);
        tvRecognition = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_recognition);

        tvWalkwayBlack = (Text) rootLayout.findComponentById(ResourceTable.Id_tv_walkway_black);
    }

    public void setupFonts() {

        // Set typeface
        tvRobotoBlack.setFont(EasyFonts.robotoBlack(this));
        tvRobotoBlackItalic.setFont(EasyFonts.robotoBlackItalic(this));
        tvRobotoBold.setFont(EasyFonts.robotoBold(this));
        tvRobotoBoldItalic.setFont(EasyFonts.robotoBoldItalic(this));
        tvRobotoItalic.setFont(EasyFonts.robotoItalic(this));
        tvRobotoLight.setFont(EasyFonts.robotoLight(this));
        tvRobotoLightItalic.setFont(EasyFonts.robotoLightItalic(this));
        tvRobotoMedium.setFont(EasyFonts.robotoMedium(this));
        tvRobotoMediumItalic.setFont(EasyFonts.robotoMediumItalic(this));
        tvRobotoRegular.setFont(EasyFonts.robotoRegular(this));
        tvRobotoThin.setFont(EasyFonts.robotoThin(this));
        tvRobotoThinItalic.setFont(EasyFonts.robotoThinItalic(this));
        tvAndroidNation.setFont(EasyFonts.androidNation(this));
        tvAndroidNationBold.setFont(EasyFonts.androidNationBold(this));
        tvAndroidNationItalic.setFont(EasyFonts.androidNationItalic(this));
        tvDroidRobot.setFont(EasyFonts.droidRobot(this));
        tvDroidSerifBold.setFont(EasyFonts.droidSerifBold(this));
        tvDroidSerifBoldItalic.setFont(EasyFonts.droidSerifBoldItalic(this));
        tvDroidSerifItalic.setFont(EasyFonts.droidSerifItalic(this));
        tvDroidSerifRegular.setFont(EasyFonts.droidSerifRegular(this));
        tvFreedom.setFont(EasyFonts.freedom(this));
        tvFunRaiser.setFont(EasyFonts.funRaiser(this));
        tvGreenAvocado.setFont(EasyFonts.greenAvocado(this));
        tvRecognition.setFont(EasyFonts.recognition(this));
        tvWalkwayBlack.setFont(EasyFonts.walkwayBlack(this));
    }

}
