## EasyFonts

## Introduction
A simple and useful openharmony library to use custom fonts in openharmony apps without adding fonts into asset/resource folder.
Also by using this library developer should not have to worry about Font object creation for every new font that he use.
EasyFonts will handle several fonts for you.

## Usage instructions
You can set number of font face provided by EasyFont to your Text by passing EasyFonts.yourFont(Context context) to setFont().

Text tvRobotoBlack = (Text)findComponentById(ResourceTable.Id_tv_roboto_black);
tvRobotoBlack.setFont(EasyFonts.robotoBlack(this));
You can also check sample project created in entry directory.

## Installation instruction
**Method 1:**
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
```
implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```
**Method 2:**
In project level build.gradle:
```
allprojects{
    repositories{
        mavenCentral()
    }
}
```
Add the following code to the entry gradle:
```
implementation project(path: ': easyfonts)
```
**Method 3:**
For using EasyFonts from remote repository in separate application, add the below dependency in "entry" build.gradle.

Modify entry build.gradle as below :

```
dependencies {
  implementation 'io.openharmony.tpc.thirdlib:EasyFonts:1.0.0'
}
```